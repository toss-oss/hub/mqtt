FROM arm64v8/node:10.16.3-buster

WORKDIR /usr/src/app
EXPOSE 1883

COPY package*.json ./

RUN npm install

COPY . .

CMD [ "node", "server.js" ]